/* beefy-window.h: Window-based HTML editor widget

   Copyright (C) 2002 Bastien Nocera <hadess@hadess.net>

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Bastien Nocera <hadess@hadess.net>
 */

#ifndef BEEFY_WINDOW_H
#define BEEFY_WINDOW_H

#include <bonobo/bonobo-window.h>

#define BEEFY_TYPE_WINDOW            (beefy_window_get_type ())
#define BEEFY_WINDOW(obj)            (GTK_CHECK_CAST ((obj), BEEFY_TYPE_WINDOW, BeefyWindow))
#define BEEFY_WINDOW_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), BEEFY_TYPE_WINDOW, BeefyWindowClass))
#define BEEFY_IS_WINDOW(obj)         (GTK_CHECK_TYPE ((obj), BEEFY_TYPE_WINDOW))
#define BEEFY_IS_WINDOW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), BEEFY_TYPE_WINDOW))

typedef struct BeefyWindow	      BeefyWindow;
typedef struct BeefyWindowClass	      BeefyWindowClass;
typedef struct BeefyWindowPrivate     BeefyWindowPrivate;

struct BeefyWindow {
	BonoboWindow parent;
	BeefyWindowPrivate *_priv;
};

struct BeefyWindowClass {
	BonoboWindowClass parent_class;

/*	void (*changed) (BeefyWindow *playlist);
	void (*current_removed) (BeefyWindow *playlist);*/
};

GtkType    beefy_window_get_type (void);
GtkWidget *beefy_window_new      (void);

gboolean beefy_window_load_file  (BeefyWindow *beefy, const gchar *filename);

#endif /* BEEFY_WINDOW_H */
