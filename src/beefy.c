/*  This file is part of the GtkHTML library.

    Copyright (C) 2000 Helix Code, Inc.
    Copyright (C) 2002 Bastien Nocera <hadess@hadess.net>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    Authors: Ettore Perazzoli <ettore@helixcode.com>
    Bastien Nocera <hadess@hadess.net>
*/

#include "config.h"

#include <gnome.h>
#include <bonobo.h>
#include "beefy-window.h"

GtkWidget *beefy;

static void
beefy_error (char *msg, GtkWindow *parent)
{
	static GtkWidget *error_dialog = NULL;

	if (error_dialog != NULL)
		return;

	error_dialog = gtk_message_dialog_new (parent,
				GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR,
				GTK_BUTTONS_OK, "%s", msg);
	gtk_dialog_set_default_response (GTK_DIALOG (error_dialog),
			GTK_RESPONSE_OK);
	gtk_widget_show (error_dialog);
	gtk_dialog_run (GTK_DIALOG (error_dialog));
	gtk_widget_destroy (error_dialog);
	error_dialog = NULL;
}

static void
app_destroy_cb (GtkWidget * app, BonoboUIContainer * uic)
{
	bonobo_object_unref (BONOBO_OBJECT (uic));

	gtk_main_quit ();
}

static int
app_delete_cb (GtkWidget * widget, GdkEvent * event, gpointer dummy)
{
	gtk_widget_destroy (GTK_WIDGET (widget));

	return FALSE;
}

static guint
container_create (void)
{
	beefy = beefy_window_new ();
	if (beefy == NULL)
		exit (0);

	gtk_widget_show (GTK_WIDGET (beefy));

	return FALSE;
}

static gint
load_file (const gchar * fname)
{
	g_print ("loading: %s\n", fname);
	beefy_window_load_file (BEEFY_WINDOW (beefy), fname);

	return FALSE;
}

int
main (int argc, char **argv)
{
	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	if (bonobo_ui_init ("beefy", "1.0", &argc, argv) == FALSE)
	{
		beefy_error ("Could not initialize Bonobo.\n"
				"Please verify your system installation.",
				NULL);
		exit (1);
	}

	bonobo_activate ();

	/* We can't make any CORBA calls unless we're in the main loop.  So we
	   delay creating the container here. */
	gtk_idle_add ((GtkFunction) container_create, NULL);
	if (argc > 1 && *argv[argc - 1] != '-')
		gtk_idle_add ((GtkFunction) load_file, argv[argc - 1]);

	bonobo_activate ();
	bonobo_main ();

	return bonobo_ui_debug_shutdown ();
}
