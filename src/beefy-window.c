/* beefy-window.c

   Copyright (C) 2002 Bastien Nocera

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Bastien Nocera <hadess@hadess.net>
 */

#define EDITOR_API_VERSION "3.0"

#include "config.h"
#include "beefy-window.h"

#include <gnome.h>
#include <bonobo.h>

#include <Editor.h>
#include "editor-control-factory.h"

struct BeefyWindowPrivate
{
	GtkWidget *control;
	gboolean format_html;
};

/* Signals */
enum {
/*	CHANGED,
	CURRENT_REMOVED,*/
	LAST_SIGNAL
};

static int beefy_window_table_signals[LAST_SIGNAL] = { 0 };

static const GtkTargetEntry target_table[] = {
	{ "text/uri-list", 0, 0 },
};

static GtkWidgetClass *parent_class = NULL;

static void beefy_window_class_init (BeefyWindowClass *class);
static void beefy_window_init       (BeefyWindow      *label);

static void open_through_persist_file_cb (GtkWidget *widget, gpointer data);
static void save_through_persist_file_cb (GtkWidget *widget, gpointer data);
static void exit_cb (GtkWidget *widget, gpointer data);

static BonoboUIVerb verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("OpenFile",   open_through_persist_file_cb),
	BONOBO_UI_UNSAFE_VERB ("SaveFile",   save_through_persist_file_cb),
	BONOBO_UI_UNSAFE_VERB ("SaveAsFile",   save_through_persist_file_cb),

	BONOBO_UI_UNSAFE_VERB ("FileExit", exit_cb),

	BONOBO_UI_VERB_END
};

GtkType
beefy_window_get_type (void)
{
	static GtkType beefy_window_type = 0;

	if (!beefy_window_type) {
		static const GTypeInfo beefy_window_info = {
			sizeof (BeefyWindowClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) beefy_window_class_init,
			(GClassFinalizeFunc) NULL,
			NULL /* class_data */,
			sizeof (BeefyWindow),
			0 /* n_preallocs */,
			(GInstanceInitFunc) beefy_window_init,
		};

		beefy_window_type = g_type_register_static (BONOBO_TYPE_WINDOW,
				"BeefyWindow", &beefy_window_info,
				(GTypeFlags)0);
	}

	return beefy_window_type;
}

static void
beefy_error (char *msg)
{
	static GtkWidget *error_dialog = NULL;

	if (error_dialog != NULL)
		return;

	error_dialog =
		gtk_message_dialog_new (NULL,
				GTK_DIALOG_MODAL,
				GTK_MESSAGE_ERROR,
				GTK_BUTTONS_OK,
				"%s", msg);
	gtk_dialog_set_default_response (GTK_DIALOG (error_dialog),
			GTK_RESPONSE_OK);
	gtk_widget_show (error_dialog);
	gtk_dialog_run (GTK_DIALOG (error_dialog));
	gtk_widget_destroy (error_dialog);
	error_dialog = NULL;
}

static void
beefy_window_init (BeefyWindow *beefy)
{
	beefy->_priv = g_new0 (BeefyWindowPrivate, 1);
	beefy->_priv->control = NULL;
	beefy->_priv->format_html = TRUE;
/*	playlist->_priv->current = NULL;
	playlist->_priv->icon = NULL;
	playlist->_priv->path = NULL;*/
}
#if 0
static void
open_or_save_as_dialog (BonoboWindow *app, FileSelectionOperation op)
{
	GtkWidget    *widget;
	BonoboWidget *control;

	control = BONOBO_WIDGET (bonobo_window_get_contents (app));

	if (file_selection_info.widget != NULL) {
		gdk_window_show (GTK_WIDGET
				(file_selection_info.widget)->window);
		return;
	}

	if (op == OP_LOAD_THROUGH_PERSIST_FILE)
		widget = gtk_file_selection_new (_("Open file..."));
	else
		widget = gtk_file_selection_new (_("Save file as..."));

	gtk_window_set_transient_for (GTK_WINDOW (widget),
			GTK_WINDOW (app));

	file_selection_info.widget = widget;
	file_selection_info.control = control;
	file_selection_info.operation = op;

	g_signal_connect_object (GTK_FILE_SELECTION (widget)->cancel_button,
			"clicked", G_CALLBACK (gtk_widget_destroy),
			widget, G_CONNECT_AFTER);
	g_signal_connect (GTK_FILE_SELECTION (widget)->ok_button,
			"clicked", G_CALLBACK (file_selection_ok_cb), NULL);
	g_signal_connect (file_selection_info.widget, "destroy",
			G_CALLBACK (file_selection_destroy_cb), NULL);

	gtk_widget_show (file_selection_info.widget);
}
#endif

static gboolean
load_through_persist_file (const gchar *filename,
		Bonobo_PersistFile pfile)
{
	CORBA_Environment ev;
	gboolean retval = TRUE;

	CORBA_exception_init (&ev);

	Bonobo_PersistFile_load (pfile, filename, &ev);

	if (ev._major != CORBA_NO_EXCEPTION)
	{
		//FIXME
		g_warning ("Cannot load.");
		retval = FALSE;
	}

	CORBA_exception_free (&ev);

	return retval;
}


static void
open_through_persist_file_cb (GtkWidget *widget, gpointer data)
{
//	open_or_save_as_dialog (BONOBO_WINDOW (data),
//			OP_LOAD_THROUGH_PERSIST_FILE);
}

static void
save_through_persist_file_cb (GtkWidget *widget, gpointer data)
{
//	open_or_save_as_dialog (BONOBO_WINDOW (data),
//			OP_SAVE_THROUGH_PERSIST_FILE);
}

static void
exit_cb (GtkWidget *widget, gpointer data)
{
	gtk_widget_destroy (GTK_WIDGET (data));
}

static gboolean
window_delete_cb (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	gtk_widget_destroy (widget);

	return FALSE;
}

static void
window_destroy_cb (GtkObject *object, gpointer user_data)
{
	gtk_widget_destroy (GTK_WIDGET (object));
}

static void
menu_format_html_cb (BonoboUIComponent           *component,
		const char                  *path,
		Bonobo_UIComponent_EventType type,
		const char                  *state,
		gpointer                     user_data)
{
	BeefyWindow *beefy;

	beefy = (BeefyWindow *) user_data;

	if (type != Bonobo_UIComponent_STATE_CHANGED)
		return;
	beefy->_priv->format_html = *state == '0' ? 0 : 1;

	bonobo_widget_set_property (BONOBO_WIDGET (beefy->_priv->control),
			"FormatHTML", TC_CORBA_boolean,
			beefy->_priv->format_html, NULL);
	bonobo_widget_set_property (BONOBO_WIDGET (beefy->_priv->control),
			"HTMLTitle", TC_CORBA_string, "testing", NULL);
}

static void
beefy_window_finalize (GObject *object)
{
	BeefyWindow *beefy;

	g_return_if_fail (BEEFY_IS_WINDOW (object));
	beefy = BEEFY_WINDOW (object);

/*	if (playlist->_priv->current != NULL)
		gtk_tree_path_free (playlist->_priv->current);
	if (playlist->_priv->icon != NULL)
		gdk_pixbuf_unref (playlist->_priv->icon);
*/
	if (G_OBJECT_CLASS (parent_class)->finalize != NULL)
		(* G_OBJECT_CLASS (parent_class)->finalize) (object);
}

GtkWidget*
beefy_window_new (void)
{
	BeefyWindow *beefy;
	GtkWindow *window;
	BonoboUIComponent *component;
	BonoboUIContainer *container;
	CORBA_Environment ev;
	GNOME_GtkHTML_Editor_Engine engine;

	/* Create the Object as a window */
	beefy = BEEFY_WINDOW (g_object_new (BEEFY_TYPE_WINDOW, NULL));
	window = GTK_WINDOW (beefy);
	gtk_window_set_title (window, _("Beefy"));
	container = bonobo_window_get_ui_container (BONOBO_WINDOW (window));

	g_signal_connect (window, "delete_event",
			G_CALLBACK (window_delete_cb), NULL);

	g_signal_connect (window, "destroy",
			G_CALLBACK (window_destroy_cb), container);

	gtk_window_set_default_size (window, 550, 450);
	gtk_window_set_policy (window, TRUE, TRUE, FALSE);

	/* Take care of Bonobo Component */
	component = bonobo_ui_component_new ("beefy");
	bonobo_running_context_auto_exit_unref (BONOBO_OBJECT (component));

	bonobo_ui_component_set_container (component,
			BONOBO_OBJREF (container), NULL);
	bonobo_ui_component_add_verb_list_with_data (component, verbs, beefy);
	if (g_file_test (DATADIR "/gnome-2.0/ui/beefy-ui.xml",
				G_FILE_TEST_EXISTS) == FALSE)
	{
		char *msg;

		msg = g_strdup_printf ("Cannot get `%s'.",
				DATADIR "/gnome-2.0/ui/beefy-ui.xml");
		beefy_error (msg);
		g_free (msg);

		return NULL;
	}

	bonobo_ui_util_set_ui (component, DATADIR, "beefy-ui.xml",
			"beefy", NULL);

	beefy->_priv->control = bonobo_widget_new_control (CONTROL_ID,
			BONOBO_OBJREF (container));

	if (beefy->_priv->control == NULL)
	{
		char *msg;

		msg = g_strdup_printf ("Cannot get `%s'.", CONTROL_ID);
		beefy_error (msg);
		g_free (msg);

		return NULL;
	}

	bonobo_widget_set_property (BONOBO_WIDGET (beefy->_priv->control),
			"FormatHTML", TC_CORBA_boolean,
			beefy->_priv->format_html, NULL);
	bonobo_ui_component_set_prop (component, "/commands/FormatHTML",
			"state", beefy->_priv->format_html ? "1" : "0", NULL);
	bonobo_ui_component_add_listener (component, "FormatHTML",
			menu_format_html_cb, beefy);

	bonobo_window_set_contents (BONOBO_WINDOW (beefy),
			beefy->_priv->control);
	gtk_widget_show (beefy->_priv->control);

	CORBA_exception_init (&ev);
	engine = (GNOME_GtkHTML_Editor_Engine) Bonobo_Unknown_queryInterface
		(bonobo_widget_get_objref
		 (BONOBO_WIDGET (beefy->_priv->control)),
		 "IDL:GNOME/GtkHTML/Editor/Engine:1.0", &ev);
	GNOME_GtkHTML_Editor_Engine_runCommand (engine, "grab-focus", &ev);
	Bonobo_Unknown_unref (engine, &ev);
	CORBA_Object_release (engine, &ev);
	CORBA_exception_free (&ev);

	return GTK_WIDGET (beefy);
}

gboolean
beefy_window_load_file (BeefyWindow *beefy, const gchar *filename)
{                    
	CORBA_Object interface;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	interface = Bonobo_Unknown_queryInterface
		(bonobo_widget_get_objref (BONOBO_WIDGET
					   (beefy->_priv->control)),
		 "IDL:Bonobo/PersistFile:1.0", &ev);
	CORBA_exception_free (&ev);

	return load_through_persist_file (filename, interface);
}

static void
beefy_window_class_init (BeefyWindowClass *klass)
{
	parent_class = gtk_type_class (bonobo_window_get_type ());

	G_OBJECT_CLASS (klass)->finalize = beefy_window_finalize;

	/* Signals */
/*	beefy_window_table_signals[CHANGED] =
		g_signal_new ("changed",
				G_TYPE_FROM_CLASS (klass),
				G_SIGNAL_RUN_LAST,
				G_STRUCT_OFFSET (BeefyWindowClass, changed),
				NULL, NULL,
				g_cclosure_marshal_VOID__VOID,
				G_TYPE_NONE, 0);
	beefy_window_table_signals[CURRENT_REMOVED] =
		g_signal_new ("current-removed",
				G_TYPE_FROM_CLASS (klass),
				G_SIGNAL_RUN_LAST,
				G_STRUCT_OFFSET (BeefyWindowClass, changed),
				NULL, NULL,
				g_cclosure_marshal_VOID__VOID,
				G_TYPE_NONE, 0);*/
}

